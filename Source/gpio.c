/********************************************************
 * This is an exercise which has been given by NXP     * 
 * For the purpose of interview                         *
 * The purpose of the GPIO file is to provide the       *
 * driver interface for enabling the pins and ports     *
 *                                                      * 
 * Author : Shrey Ravindra                              *
 * Organization : NXP Assignment                        *
 *                                                      * 
 ********************************************************
 */ 
#include "gpio.h"

/********************************************************
 * This section contains a decleration of static 
 * functions. These are functions which are used by the 
 * driver only. These are not accessed by the function
 * outside of the driver. Description of the function
 * will be in the 
 ********************************************************
 */
static bool lpc29xx_isPinOk(GPIO_Reg* GPIOx, uint32_t GPIO_Pin);
static bool lpc29xx_isRegOk(GPIO_Reg* GPIOx);
static bool lpc29xx_isMultipleBitsSet(uint32_t mask);
static bool lpc29xx_isDirectionOk(GPIO_Reg* GPIOx, uint32_t mask);

/**
  * @brief  Sets the pin direction as either input or output
  * @param  GPIOx where x can be 0...3 to select the GPIO peripheral for LPC29xx
  * @param  GPIO_Pin specifies the port bit to set.
  *         This parameter can be GPIO_PIN_x where x can be (0..31).
  * @param  GPIO_Direction specifies the direction as input/output
  * @retval void
  */
void LPC29xx_SetGpioPinDirection(GPIO_Reg* GPIOx, uint32_t GPIO_Pin, GPIO_PinDirection GPIO_Direction)
{
    uint32_t mask = 0x0;
    uint32_t temp_value = 0x0; 
    assert(NULL != GPIOx); 
    assert(lpc29xx_isPinOk(GPIOx,GPIO_Pin) == true); 

    /*Read the value in the register*/
    temp_value = GPIOx->DR; 
    if(GPIO_PIN_OUTPUT == GPIO_Direction)
    {
        mask = GPIO_Pin; 
        /*set the pins which are needed to be set*/ 
        temp_value = (temp_value|mask); 
        GPIOx->DR = temp_value; 
    }
    else
    {
        mask = ~GPIO_Pin; 
        temp_value = (temp_value&mask); 
        GPIOx->DR = temp_value; 
    }
    return; 
}

/**
  * @brief  Reads the GPIO port Input Register
  * @param  GPIOx where x can be 0...3 to select the GPIO peripheral for LPC29xx
  * @retval uint32_t the 32 bit value of the register
  */
uint32_t LPC29xx_ReadGpioPort(GPIO_Reg* GPIOx)
{
    assert(NULL != GPIOx);
    assert(lpc29xx_isRegOk(GPIOx) == true); 
    return (GPIOx->PINS); 
}

/**
  * @brief  Writes a 32-bit value to the output register OR
  * @param  GPIOx where x can be 0...3 to select the GPIO peripheral for LPC29xx
  * @param  value is the 32-bit value which can be written into the port.
  * @retval void
  */
void LPC29xx_WriteGpioPort(GPIO_Reg* GPIOx, uint32_t value)
{
    uint32_t mask = 0x0; 
    assert (NULL != GPIOx); 
    assert(lpc29xx_isRegOk(GPIOx) == true); 
    /*Now a 32-bit cannot be written directly for some of the ports*/
    if(GPIOx == GPIO_2)
    {
        mask = 0x0FFFFFFF;
        /*mask the last 4 bits, they are not available in LPC2901*/
        value = value & mask; 
    }
    else if (GPIOx == GPIO_3)
    {
        mask = 0x0000FFFF; 
        /*Only 16-bits are available for LPC2901*/
        value = value & mask; 
    }
    /*by default don't modify the 32-bits as they are valid*/
    GPIOx->OR = value;
    return; 
}
/**
  * @brief  Reads the individual pins for the PINS
  * @param  GPIOx where x can be 0...3 to select the GPIO peripheral for LPC29xx
  * @param  GPIO_Pin is the bit mask for a single pin and not multiple Pins.
  * @retval GPIO_PinState returns the state of the PINS register as Set/Reset
  */
GPIO_PinState LPC29xx_ReadGpioPin(GPIO_Reg* GPIOx, uint32_t GPIO_Pin)
{
    uint32_t value =0x0; 
    assert (NULL != GPIOx); 
    assert(lpc29xx_isRegOk(GPIOx) == true);
    assert(lpc29xx_isMultipleBitsSet(GPIO_Pin) == false); 
    value = GPIOx->PINS; 
    value = value & GPIO_Pin; /*GPIO pin acts as a mask*/
    if(value == 0x0)
    {
        return GPIO_PIN_RESET; 
    }
    else
    {
        return GPIO_PIN_SET; 
    }
}

/**
  * @brief  Writes the individual pins for the OR
  * @param  GPIOx where x can be 0...3 to select the GPIO peripheral for LPC29xx
  * @param  GPIO_Pin is the bit mask for a single pin and not multiple Pins.
  * @param  GPIO_State is the state of the pin (SET or RESET)
  * @retval void
  */
void LPC29xx_WriteGpioPin(GPIO_Reg* GPIOx, uint32_t GPIO_Pin, GPIO_PinState GPIO_State)
{
    uint32_t value = 0x0; 
    uint32_t mask = 0x0; 
    assert(lpc29xx_isRegOk(GPIOx) == true);
    assert(lpc29xx_isPinOk(GPIOx,GPIO_Pin) == true);
    assert(lpc29xx_isMultipleBitsSet(GPIO_Pin) == false); 
    assert(lpc29xx_isDirectionOk(GPIOx, GPIO_Pin) == true);
    value = GPIOx->OR; 
    if(GPIO_PIN_SET == GPIO_State)
    {
        mask = GPIO_Pin;
        value = (value|mask); 
        GPIOx->OR = value; 
    }
    else
    {
        mask = ~GPIO_Pin; 
        value = (value & mask); 
        GPIOx->OR = value; 
    }
    return;
}

/**
  * @brief  Reads the individual pins for the Pin Direction
  * @param  GPIOx where x can be 0...3 to select the GPIO peripheral for LPC29xx
  * @param  GPIO_Pin is the bit mask for a single pin and not multiple Pins.
  * @retval GPIO_PinDirection returns the direction of the Pin, Input or Output
  */
GPIO_PinDirection LPC29xx_ReadGpioPinDirection(GPIO_Reg* GPIOx, uint32_t GPIO_Pin)
{
    uint32_t value =0x0; 
    assert (NULL != GPIOx); 
    assert(lpc29xx_isRegOk(GPIOx) == true);
    assert(lpc29xx_isMultipleBitsSet(GPIO_Pin) == false); 
    value = GPIOx->DR; 
    value = value & GPIO_Pin; /*GPIO pin acts as a mask*/
    if(value == 0x0)
    {
        return GPIO_PIN_INPUT; 
    }
    else
    {
        return GPIO_PIN_OUTPUT; 
    }
}

/**
  * @brief  Simulates a Pin Reset Scenario for the GPIO, which cannot be done
  *         in a PC
  * @param  GPIOx where x can be 0...3 to select the GPIO peripheral for LPC29xx
  * @retval void
  */
void LPC29xx_SimulateGpioReset(GPIO_Reg* GPIOx)
{
    GPIOx->OR = 0x0; 
    GPIOx->DR = 0x0; 
}

#ifdef PRINT_DEBUG_MESSAGES_IN_DRIVER
/**
  * @brief  Prints the Register Values of the Control Register
  * @param  GPIOx where x can be 0...3 to select the GPIO peripheral for LPC29xx
  * @retval void
  */
void LPC29xx_PrintRegisterValues(GPIO_Reg* GPIOx)
{
    assert (NULL != GPIOx); 
    assert(lpc29xx_isRegOk(GPIOx) == true);
    printf("\nPINS = 0x%x",GPIOx->PINS); 
    printf("\n OR = 0x%x",GPIOx->OR); 
    printf("\n DR= 0x%x", GPIOx->DR);
    return;
}
#endif /*PRINT_DEBUG_MESSAGES_IN_DRIVER*/

/********************************************************
 * This section contains a definition of static 
 * functions. These are functions which are used by the 
 * driver only. These are not accessed by the function
 * outside of the driver. Description of the function
 * will be in the 
 ********************************************************
 */
bool lpc29xx_isDirectionOk(GPIO_Reg* GPIOx, uint32_t mask)
{
    uint32_t value = 0x0; 
    value = GPIOx->DR; 
    value = value & mask; 
    if(value == 0x0)
    {
        return false; 
    }
    else
    {
        return true;
    }
}
bool lpc29xx_isMultipleBitsSet(uint32_t mask)
{
    uint32_t count = 0x0; 
    uint8_t i = 0; 
    uint32_t temp = 0x0; 
    for(i =0; i<=31; i++)
    {
        temp = mask & 0x1; 
        if(temp == 0x1)
        {
            count++; 
            temp= temp^temp; 
            mask = mask >> 0x1; 
        }
        else
        {
            mask = mask >> 0x1;
        }
    }
    if(count > 1)
    {
        return true; 
    }
    else
    {
        return false;
    }

}
bool lpc29xx_isRegOk(GPIO_Reg* GPIOx)
{
#ifdef MACINTOSH
    return true; 
#else
#if  (defined LPC2901) || (defined LPC2917) || (defined LPC2919) || (defined LPC2926) || (defined LPC2927) || (defined LPC2929) || (defined LPC2930) || defined (LPC2939)
    if(GPIOx == GPIO_0 || GPIOx == GPIO_1 || GPIOx == GPIO_2 || GPIOx == GPIO_3)
    {
        return true; 
    }
    else
    {
        return false;
    }
#endif
#endif /*MACINTOSH*/
}


bool lpc29xx_isPinOk(GPIO_Reg* GPIOx, uint32_t GPIO_Pin)
{
    bool flag = true; 
    uint32_t temp = 0x0; 
#ifdef MACINTOSH
    flag = true;
#else
#if  (defined LPC2901) || (defined LPC2917) || (defined LPC2919) || (defined LPC2926) || (defined LPC2927) || (defined LPC2929) || (defined LPC2930) || defined (LPC2939)
    if(GPIOx == GPIO_0 || GPIOx == GPIO_1 || GPIOx == GPIO_2 || GPIOx == GPIO_3)
    {
        flag = true; 
    }
    else
    {
        flag = false;
    }
#endif 
#endif /*MACINTOSH*/
    if(0x00000000 == GPIO_Pin)
    {
        flag = false; 
    }
    /*check if the appropriate pins are available in Port*/
    if(GPIOx == GPIO_2)
    {
        temp = (GPIO_Pin & 0xF0000000); 
        if(temp!= 0x0)
        {
            flag = false; 
        } 
        temp = temp^temp; /*clear temp register*/
    }
    if(GPIOx == GPIO_3)
    {
        temp = (GPIO_Pin & 0xFFFF0000); 
        if(temp!= 0x0)
        {
            flag = false; 
        }
    }
    return flag; 
}
