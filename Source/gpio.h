/********************************************************
 * This is an exercise which has been given by NXP     * 
 * For the purpose of interview                         *
 * The purpose of the GPIO file is to provide the       *
 * driver interface for enabling the pins and ports     *
 *                                                      * 
 * Author : Shrey Ravindra                              *
 * Organization : NXP Assignment                        *
 *                                                      * 
 ********************************************************
 */


#ifndef __GPIO_H__
#define __GPIO_H__

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <assert.h>

#include "gpio_hwio.h"

#if PRINT_DEBUG_MESSAGES_IN_DRIVER
#define PRNT(a) printf a
#else 
#define PRNT(a) (void)0x0
#endif /*PRINT_DEBUG_MESSAGES_IN_DRIVER*/ 

#define GPIO_PIN_0                      ((uint32_t)0x00000001)
#define GPIO_PIN_1                      ((uint32_t)0x00000002)
#define GPIO_PIN_2                      ((uint32_t)0x00000004)
#define GPIO_PIN_3                      ((uint32_t)0x00000008)
#define GPIO_PIN_4                      ((uint32_t)0x00000010)
#define GPIO_PIN_5                      ((uint32_t)0x00000020)
#define GPIO_PIN_6                      ((uint32_t)0x00000040)
#define GPIO_PIN_7                      ((uint32_t)0x00000080)
#define GPIO_PIN_8                      ((uint32_t)0x00000100)
#define GPIO_PIN_9                      ((uint32_t)0x00000200)
#define GPIO_PIN_10                     ((uint32_t)0x00000400)
#define GPIO_PIN_11                     ((uint32_t)0x00000800)
#define GPIO_PIN_12                     ((uint32_t)0x00001000)
#define GPIO_PIN_13                     ((uint32_t)0x00002000)
#define GPIO_PIN_14                     ((uint32_t)0x00004000)
#define GPIO_PIN_15                     ((uint32_t)0x00008000)
#define GPIO_PIN_16                     ((uint32_t)0x00010000)
#define GPIO_PIN_17                     ((uint32_t)0x00020000)
#define GPIO_PIN_18                     ((uint32_t)0x00040000)
#define GPIO_PIN_19                     ((uint32_t)0x00080000)
#define GPIO_PIN_20                     ((uint32_t)0x00100000)
#define GPIO_PIN_21                     ((uint32_t)0x00200000)
#define GPIO_PIN_22                     ((uint32_t)0x00400000)
#define GPIO_PIN_23                     ((uint32_t)0x00800000)
#define GPIO_PIN_24                     ((uint32_t)0x01000000)
#define GPIO_PIN_25                     ((uint32_t)0x02000000)
#define GPIO_PIN_26                     ((uint32_t)0x04000000)
#define GPIO_PIN_27                     ((uint32_t)0x08000000)
#define GPIO_PIN_28                     ((uint32_t)0x10000000)
#define GPIO_PIN_29                     ((uint32_t)0x20000000)
#define GPIO_PIN_30                     ((uint32_t)0x40000000)
#define GPIO_PIN_31                     ((uint32_t)0x80000000)
#define GPIO_PIN_ALL                    ((uint32_t)0xFFFFFFFF)


/*Pin state of the GPIO used in the driver*/
typedef enum 
{
    GPIO_PIN_RESET = 0U, 
    GPIO_PIN_SET
} GPIO_PinState; 

/*Pin direction of the GPIO*/
typedef enum 
{
    GPIO_PIN_INPUT = 0U, 
    GPIO_PIN_OUTPUT, 
} GPIO_PinDirection; 

/*Functions for performing the GPIO Operations */
uint32_t LPC29xx_ReadGpioPort(GPIO_Reg* GPIOx); 
void LPC29xx_WriteGpioPort(GPIO_Reg* GPIOx, uint32_t value); 
GPIO_PinState LPC29xx_ReadGpioPin(GPIO_Reg* GPIOx, uint32_t GPIO_Pin); 
void LPC29xx_WriteGpioPin(GPIO_Reg* GPIOx, uint32_t GPIO_Pin, GPIO_PinState GPIO_State); 
void LPC29xx_SetGpioPinDirection(GPIO_Reg* GPIOx, uint32_t GPIO_Pin, GPIO_PinDirection GPIO_Direction); 
GPIO_PinDirection LPC29xx_ReadGpioPinDirection(GPIO_Reg* GPIOx, uint32_t GPIO_Pin);
void LPC29xx_SimulateGpioReset(GPIO_Reg* GPIOx);

#ifdef PRINT_DEBUG_MESSAGES_IN_DRIVER
void LPC29xx_PrintRegisterValues(GPIO_Reg* GPIOx); 
#endif /*PRINT_DEBUG_MESSAGES_IN_DRIVER*/

#endif  /* __GPIO_H__*/


