/********************************************************
 * This is an exercise which has been given by NXP     * 
 * For the purpose of interview                         *
 * The purpose of the GPIO file is to provide the       *
 * driver interface for enabling the pins and ports     *
 *                                                      * 
 * Author : Shrey Ravindra                              *
 * Organization : NXP Assignment                        *
 *                                                      * 
 ********************************************************
 */


#ifndef __GPIO_HWIO_H__
#define __GPIO_HWIO_H__  

#include<stdint.h> /* for standard typedef definitions for 8-bit, 16-bit, 32-bit numbers */                   

#define LPC29xx_GPIO_BASE_ADDR                      0xE004A000  
#define LPC29xx_GPIO_REG_OFFSET                     0x1000
#define LPC29xx_GPIO_0_BASE_ADDR                    (LPC29xx_GPIO_BASE_ADDR)
#define LPC29xx_GPIO_1_BASE_ADDR                    (LPC29xx_GPIO_0_BASE_ADDR + LPC29xx_GPIO_REG_OFFSET)

/*Currently the driver only supports LPC2901 */
#if  (defined LPC2901) || (defined LPC2917) || (defined LPC2919) || (defined LPC2926) || (defined LPC2927) || (defined LPC2929) || (defined LPC2930) || defined (LPC2939)
#define LPC29xx_GPIO_2_BASE_ADDR                    (LPC29xx_GPIO_1_BASE_ADDR + LPC29xx_GPIO_REG_OFFSET)
#define LPC29xx_GPIO_3_BASE_ADDR                    (LPC29xx_GPIO_2_BASE_ADDR + LPC29xx_GPIO_REG_OFFSET)
#endif /* HWIO maps for LPC2901/17/19/26/27/29/30/39 family of processors only*/               

#if (defined LPC2930) || defined (LPC2939)
#define LPC29xx_GPIO_4_BASE_ADDR                    (LPC29xx_GPIO_3_BASE_ADDR + LPC29xx_GPIO_REG_OFFSET)
#define LPC29xx_GPIO_5_BASE_ADDR                    (LPC29xx_GPIO_4_BASE_ADDR + LPC29xx_GPIO_REG_OFFSET)
#endif /*HWIO Maps for LPC2930/39 family of processors*/

/*Register definition of the driver */
typedef struct 
{
    volatile uint32_t PINS; /*Port Input Register, Read-Only and is at Offset 0x0 from Base address */
    volatile uint32_t OR;  /* Port Output Register, R/W access and is at Offset 0x4 from Base address */
    volatile uint32_t DR;  /* Port Direction Register, R/W access and is at Offset 0x8 from Base Address */ 
}GPIO_Reg;  

#define GPIO_0              ((GPIO_Reg*)LPC29xx_GPIO_0_BASE_ADDR)
#define GPIO_1              ((GPIO_Reg*)LPC29xx_GPIO_1_BASE_ADDR)

#if  (defined LPC2901) || (defined LPC2917) || (defined LPC2919) || (defined LPC2926) || (defined LPC2927) || (defined LPC2929) || (defined LPC2930) || defined (LPC2939)
#define GPIO_2              ((GPIO_Reg*)LPC29xx_GPIO_2_BASE_ADDR)
#define GPIO_3              ((GPIO_Reg*)LPC29xx_GPIO_3_BASE_ADDR)
#endif 

#if (defined LPC2930) || defined (LPC2939)
#define GPIO_4              ((GPIO_Reg*)LPC29xx_GPIO_4_BASE_ADDR)
#define GPIO_5              ((GPIO_Reg*)LPC29xx_GPIO_5_BASE_ADDR)
#endif

 
#endif  /* __GPIO_H__*/
