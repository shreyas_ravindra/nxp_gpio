/********************************************************
 *  This is an exercise which has been given by NXP     * 
 * For the purpose of interview                         *
 * The purpose of the Main C program is to act as test  *
 * case to evaluate and see if all the pins of the      *
 * GPIO driver are working as expected                  *
 *                                                      * 
 * Author : Shrey Ravindra                              *
 * Organization : NXP Assignment                        *
 *                                                      * 
 ********************************************************
 */ 

#include "main.h"
#include "gpio.h"

/********************************************************
 *  This is the main program which is the starting      * 
 *  point of execution of the test cases                *
 *                                                      * 
 ********************************************************
 */ 

/*The addresses of the registers do not map correctly on Macintosh */
/*For the purpose of testing the driver, providing addresses locally */
/* In the actual driver the GPIO registers are memory mapped registers */
/* So we can use the addresses as needed */

#ifdef MACINTOSH
GPIO_Reg Gpio0; 
GPIO_Reg Gpio1; 
GPIO_Reg Gpio2; 
GPIO_Reg Gpio3; 
#endif /*MACINTOSH*/

int main(void)
{
    printf("\n***   WELCOME TO THE TEST CASE EXECUTION TO CHECK NVM DRIVER   ***\n");
    printf("\n 1. Set GPIO_0 as ports Output and Set All Pins as Logical High"); 
    Test_Case_1(); 
    printf(".....PASSED\n");

    printf("\n 2. Set GPIO_1 as ports Output and Set All Pins as Logical High"); 
    Test_Case_2(); 
    printf(".....PASSED\n");

    printf("\n 3. Set GPIO_2 as ports Output and set All pins as Logical High");
    Test_Case_3();
    printf(".....PASSED\n");

    printf("\n 4. Set GPIO_3 as ports Output and set All pins as Logical High");
    Test_Case_4(); 
    printf(".....PASSED\n");

    printf("\n 5. Driver Example 1- Pins 1-10 as Input and Pins 11-15 as output using GPIO 0");
    LPC29xx_SimulateGpioReset(&Gpio0); /*Step needed for driver verification as pins can't be reset on PC*/
    driver_example_1(); 
    printf(".....PASSED\n");

    printf("\n6. Driver Example 2 - Pins 28-31 as Input and Pins 0-7 as output using GPI0 1");
    LPC29xx_SimulateGpioReset(&Gpio1); /*Step needed for driver verification as pins can't be reset on PC*/
    driver_example_2(); 
    printf(".....PASSED\n");

    printf("\n7. Driver Example 3 -- Changing the PIN Configurations");
    printf("\n Pins 28-31 as Input and Pins 0-7 as output using GPIO 1"); 
    printf("\n Pins 28-31 as Output and Pins 0-7 as input using GPIO 1");
    LPC29xx_SimulateGpioReset(&Gpio1); /*Step needed for driver verification as pins can't be reset on PC*/
    driver_example_3(); 
    printf(".....PASSED\n");
    
    return 0x0; 
}
/********************************************************
 *  This is the area which has all the tests which makes*
 *  use of the GPIO APIs to read and write the Pins     *
 *                                                      * 
 ********************************************************
 */ 
void Test_Case_1()
{
    uint8_t i =0; 
    bool flag = true; 
    uint32_t mask = 0x00000001; 
    LPC29xx_SetGpioPinDirection(&Gpio0, GPIO_PIN_ALL,GPIO_PIN_OUTPUT); 
    LPC29xx_WriteGpioPort(&Gpio0,0xffffffff);
    for(i=0; i<=31; i++)
    {
       if(GPIO_PIN_OUTPUT == LPC29xx_ReadGpioPinDirection(&Gpio0,mask))
       {
            mask = mask << 0x1; 
            continue;
       }
       else
       {
            flag = false;
       }
    }
    assert(flag == true);
}

void Test_Case_2()
{
    uint8_t i =0; 
    bool flag = true;
    uint32_t mask = 0x00000001; 
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_ALL,GPIO_PIN_OUTPUT); 
    LPC29xx_WriteGpioPort(&Gpio1,0xffffffff); 
    for(i=0; i<=31; i++)
    {
       if(GPIO_PIN_OUTPUT == LPC29xx_ReadGpioPinDirection(&Gpio1,mask))
       {
            mask = mask << 0x1; 
            continue;
       }
       else
       {
            flag = false;
       }
    }
    assert(flag == true); 
}

void Test_Case_3()
{
    /*On Macintosh this test case will pass. On actual hardware, when the address used is
     * GPIO_2, this test case should fail.
     */
    uint8_t i =0; 
    bool flag = true;
    uint32_t mask = 0x00000001; 
    LPC29xx_SetGpioPinDirection(&Gpio2, GPIO_PIN_ALL,GPIO_PIN_OUTPUT); 
    LPC29xx_WriteGpioPort(&Gpio2,0xffffffff); 
    for(i=0; i<=31; i++)
    {
       if(GPIO_PIN_OUTPUT == LPC29xx_ReadGpioPinDirection(&Gpio2,mask))
       {
            mask = mask << 0x1; 
            continue;
       }
       else
       {
            flag = false;
       }
    }
    assert(flag == true); 
}

void Test_Case_4()
{
    /*On Macintosh this test case will pass. On actual hardware, when the address used is
     * GPIO_2, this test case should fail.
     */
    uint8_t i =0; 
    bool flag = true;
    uint32_t mask = 0x00000001; 
    LPC29xx_SetGpioPinDirection(&Gpio3, GPIO_PIN_ALL,GPIO_PIN_OUTPUT); 
    LPC29xx_WriteGpioPort(&Gpio3,0xffffffff); 
    for(i=0; i<=31; i++)
    {
       if(GPIO_PIN_OUTPUT == LPC29xx_ReadGpioPinDirection(&Gpio3,mask))
       {
            mask = mask << 0x1; 
            continue;
       }
       else
       {
            flag = false;
       }
    }
    assert(flag == true); 
}

void driver_example_1()
{
    uint32_t mask = 0x0;
    uint32_t value = 0x0; 
    /*Set Pin Direction as Input for the Pins 1-10*/
    mask |= (GPIO_PIN_1| GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5);
    mask |= (GPIO_PIN_6| GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10); 
    LPC29xx_SetGpioPinDirection(&Gpio0, mask ,GPIO_PIN_INPUT);
    /*Clear the mask for setting direction of Output pins*/
    mask = mask^mask; /*clears the masks contents */
    mask |= (GPIO_PIN_11| GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14| GPIO_PIN_15); 
    LPC29xx_SetGpioPinDirection(&Gpio0, mask ,GPIO_PIN_OUTPUT);
    /*Configure the Output pins as needed*/
    LPC29xx_WriteGpioPin(&Gpio0,GPIO_PIN_11,GPIO_PIN_SET); 
    LPC29xx_WriteGpioPin(&Gpio0,GPIO_PIN_12,GPIO_PIN_SET); 
    LPC29xx_WriteGpioPin(&Gpio0,GPIO_PIN_13,GPIO_PIN_RESET);
    LPC29xx_WriteGpioPin(&Gpio0,GPIO_PIN_14,GPIO_PIN_SET); 
    LPC29xx_WriteGpioPin(&Gpio0,GPIO_PIN_15,GPIO_PIN_SET); 

    /*Verify if indeed the values are received as needed*/
    /*This part is not needed to be implemented by the user of the driver*/
    Gpio0.PINS = 0xf0; /*This is needed as there is no way to generate the data for input port*/
    /*Read if the Input port reads the correct value*/
    value = LPC29xx_ReadGpioPort(&Gpio0); 
    assert(value == 0xf0); 
    /*Check if OR register indeed has the value set*/
    assert(Gpio0.OR == 0x0000D800); 
    assert(Gpio0.DR == 0x0000F800); 
    return; 
}

void driver_example_2()
{
    uint32_t mask = 0x0; 
    /*Set Pins 28-31 as input*/
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_28, GPIO_PIN_INPUT);
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_29, GPIO_PIN_INPUT); 
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_30, GPIO_PIN_INPUT); 
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_31, GPIO_PIN_INPUT);
    /*Set Pins 0-7 as Output*/
    mask = 0xFF; 
    LPC29xx_SetGpioPinDirection(&Gpio1, mask, GPIO_PIN_OUTPUT); 
    /*make sure alternate output ports are Logical high*/
    /*Clear the previous mask*/
    mask = 0xAA; 
    LPC29xx_WriteGpioPort(&Gpio1,mask);

    /*Verify if indeed the values are received as needed*/
    /*This part is not needed to be implemented by the user of the driver*/
    Gpio1.PINS = 0x80000000; /*This is needed as there is no way to generate the data for input port*/
    /*Read if the Input port reads the correct value*/
    assert(Gpio1.DR == 0x000000FF); 
    assert(Gpio1.OR == 0xAA); 
    assert(GPIO_PIN_SET == LPC29xx_ReadGpioPin(&Gpio1,GPIO_PIN_31)); 
    assert(GPIO_PIN_RESET == LPC29xx_ReadGpioPin(&Gpio1,GPIO_PIN_30)); 
    assert(GPIO_PIN_RESET == LPC29xx_ReadGpioPin(&Gpio1,GPIO_PIN_29));
    assert(GPIO_PIN_RESET == LPC29xx_ReadGpioPin(&Gpio1,GPIO_PIN_29));
    return;
}

void driver_example_3()
{
    uint32_t mask = 0x0; 
    uint32_t value = 0x0; 
    /*Set Pins 28-31 as input*/
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_28, GPIO_PIN_INPUT);
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_29, GPIO_PIN_INPUT); 
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_30, GPIO_PIN_INPUT); 
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_31, GPIO_PIN_INPUT);
    /*Set Pins 0-7 as Output*/
    mask = 0xFF; 
    LPC29xx_SetGpioPinDirection(&Gpio1, mask, GPIO_PIN_OUTPUT); 

    /*Reconfigure the Pins*/
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_28, GPIO_PIN_OUTPUT);
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_29, GPIO_PIN_OUTPUT); 
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_30, GPIO_PIN_OUTPUT); 
    LPC29xx_SetGpioPinDirection(&Gpio1, GPIO_PIN_31, GPIO_PIN_OUTPUT);
    /*Set Pins 0-7 as Input*/
    mask = 0xFF; 
    LPC29xx_SetGpioPinDirection(&Gpio1, mask, GPIO_PIN_INPUT);
    /*Write to GPIO Output Port*/
    mask = 0x80000000;
    LPC29xx_WriteGpioPort(&Gpio1, mask);

    /*Verify if indeed the values are received as needed*/
    /*This part is not needed to be implemented by the user of the driver*/
    Gpio1.PINS = 0x00000088; /*This is needed as there is no way to generate the data for input port*/
    /*Read if the Input port reads the correct value*/
    assert(Gpio1.OR == 0x80000000); 
    /*Verify if the Pin directions are properly set*/
    assert(GPIO_PIN_OUTPUT == LPC29xx_ReadGpioPinDirection(&Gpio1, GPIO_PIN_28));
    assert(GPIO_PIN_OUTPUT == LPC29xx_ReadGpioPinDirection(&Gpio1, GPIO_PIN_29));
    assert(GPIO_PIN_OUTPUT == LPC29xx_ReadGpioPinDirection(&Gpio1, GPIO_PIN_30));
    assert(GPIO_PIN_OUTPUT == LPC29xx_ReadGpioPinDirection(&Gpio1, GPIO_PIN_31));    
    /*Verify if Input pins are correctly configured */
    assert(GPIO_PIN_INPUT == LPC29xx_ReadGpioPinDirection(&Gpio1, GPIO_PIN_0));
    assert(GPIO_PIN_INPUT == LPC29xx_ReadGpioPinDirection(&Gpio1, GPIO_PIN_1));
    assert(GPIO_PIN_INPUT == LPC29xx_ReadGpioPinDirection(&Gpio1, GPIO_PIN_2));
    assert(GPIO_PIN_INPUT == LPC29xx_ReadGpioPinDirection(&Gpio1, GPIO_PIN_3));
    assert(GPIO_PIN_INPUT == LPC29xx_ReadGpioPinDirection(&Gpio1, GPIO_PIN_4));
    assert(GPIO_PIN_INPUT == LPC29xx_ReadGpioPinDirection(&Gpio1, GPIO_PIN_5));
    assert(GPIO_PIN_INPUT == LPC29xx_ReadGpioPinDirection(&Gpio1, GPIO_PIN_6));
    assert(GPIO_PIN_INPUT == LPC29xx_ReadGpioPinDirection(&Gpio1, GPIO_PIN_7));
    value = LPC29xx_ReadGpioPort(&Gpio1); 
    assert(value == 0x00000088); 
    return; 
}

