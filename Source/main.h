/********************************************************
 *  This is an exercise which has been given by NXP     * 
 * For the purpose of interview                         *
 * The purpose of the Main C program is to act as test  *
 * case to evaluate and see if all the pins of the      *
 * GPIO driver are working as expected                  *
 *                                                      * 
 * Author : Shrey Ravindra                              *
 * Organization : NXP Assignment                        *
 *                                                      * 
 ********************************************************
 */ 

#ifndef __MAIN_H__
#define __MAIN_H__

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

/*Declare all the test functions of the GPIO driver*/
void Test_Case_1();
void Test_Case_2();
void Test_Case_3();
void Test_Case_4();
void driver_example_1();
void driver_example_2();
void driver_example_3(); 

#endif /* __MAIN_H__*/ 
